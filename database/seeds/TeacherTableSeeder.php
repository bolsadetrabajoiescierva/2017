<?php

use App\Teacher;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class TeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create("es_ES");


        // CREACION DEL USUARIO TEACHER
            $user_opts = [
                "email" => "teacher@gmail.com",
                "password" => "123456",
                "name" => $faker->name,
                "phone" => $faker->randomNumber(9),
                "is_active" => 1
            ];

            $user = new User($user_opts);
            $user->save();

            $teacher_opts = [

                "apellidos" => $faker->lastName,
                "nrp_expediente" => $faker->randomNumber(9),
                "is_admin" => false,

            ];
            $user->teacher()->create($teacher_opts);

        // CREACION DEL USUARIO ADMIN

            $user_opts = [
                "email" => "admin@gmail.com",
                "password" => "123456",
                "name" => $faker->name,
                "phone" => $faker->randomNumber(9),
                "is_active" => 1
            ];


            $user = new User($user_opts);
            $user->save();

            $teacher_opts = [

                "apellidos" => $faker->lastName,
                "nrp_expediente" => $faker->randomNumber(9),
                "is_admin" => true,

            ];
            $user->teacher()->create($teacher_opts);



        // CREACION MASIVA DE PROFESORES
        for($i = 0; $i < 50; $i++){

            $user_opts = [
                "email" => $faker->email,
                "password" => "123456",
                "name" => $faker->name,
                "phone" => $faker->randomNumber(9),
                "is_active" => $faker->randomElement([0,1,1,1,1])
            ];


            $user = new User($user_opts);
            $user->save();

            $teacher_opts = [

                "apellidos" => $faker->lastName,
                "nrp_expediente" => $faker->randomNumber(9),
                "is_admin" => $faker->randomElement([true,false]),

            ];
            $user->teacher()->create($teacher_opts);

        }


    }
}
